//Spreadsheet function
$(document).ready(function () {

    var container = document.getElementById('excel');

    var dataSport = [
        [1, "Los Angeles Lakers", "2600", "0.93", "0.02", "293", "104.1"],
        [2, "New York Knicks", "2500", "0.79", "0.00", "278", "53.4"],
        [3, "Chicago Bulls", "2000", "0.100", "0.03", "201", "65.3"],
        [4, "Boston Celtics", "1700", "0.94", "0.09", "173", "54.9"],
        [5, "Los Angeles Clippers", "1600", "0.178", "0.00", "146", "20.1"],
        [6, "Brooklyn Nets", "1500", "0.92", "0.019", "212", "-99.4"]
    ];

    var hot = new Handsontable(container, {
        data: dataSport,
        height: 600,
        colHeaders: ["Rank", "Team", "Current Value ($mil)", "1-Yr Value Change (%)", "Debt/Value (%)", "Revenue ($mil)", "Operating Income ($mil)"],
        rowHeaders: false,
        stretchH: 'all',
        columnSorting: true,
        contextMenu: true,
        autoWrapRow: true,
        columns: [
            {data: 0, type: 'numeric'},
            {data: 1, type: 'text'},
            {data: 2, type: 'numeric', format: '$0,0.00'},
            {data: 3, type: 'numeric', format: '0.00%'},
            {data: 4, type: 'numeric', format: '0.00%'},
            {data: 5, type: 'numeric', format: '$0,0.00'},
            {data: 6, type: 'numeric', format: '$0,0.00'}
        ],
        cells: function (row, col, prop) {
            var cellProperties = {};
            cellProperties.className = 'htMiddle htCenter';
            return cellProperties;
        }
    });

});
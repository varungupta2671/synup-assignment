//Television Script
var VOLUME = 40;
$('#powerBtn').on('click', function () {
    $('.power-window').fadeToggle();
    $(this).toggleClass('btn-on')
});

$('.on-off').on('click', function () {
    $('.television-inner').toggleClass('tv-on')
});

$('.vol-up').click(function () {
    $('.volume').fadeIn();
    $('.volume-ctrl').html(VOLUME);
    setTimeout(function () {
        $('.volume').fadeOut();
    }, 2500);
    if (VOLUME < 100) {
        VOLUME++;
    }
});

$('.vol-down').click(function () {
    $('.volume').fadeIn();
    $('.volume-ctrl').html(VOLUME);
    setTimeout(function () {
        $('.volume').fadeOut();
    }, 2500);
    if (VOLUME > 1) {
        VOLUME--;
    }
});